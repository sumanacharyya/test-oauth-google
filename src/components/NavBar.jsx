import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { UserAuth } from "../context/AuthContext";

const NavBar = () => {
  const {  user } = UserAuth();

  useEffect(() => {
  }, [user]);

  return (
    <div className="flex justify-between bg-gray-400 w-full p-4">
      <h1 className="text-center text-2xl font-bold">Google Firebase OAuth.</h1>

      {user?.email ? (
        <p className="cursor-pointer">{user.email}</p>
      ) : (
        <Link to={"login"}>Login</Link>
      )}
    </div>
  );
};

export default NavBar;
