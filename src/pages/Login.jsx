import React, { useEffect } from "react";
import { FcGoogle } from "react-icons/fc";
import { UserAuth } from "../context/AuthContext";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const { googleSignIn, user } = UserAuth();

  const navigate = useNavigate();

  useEffect(() => {
    if ( user != null) {
      navigate("/account");
    }
  }, [user]);

  const googleLogInHandler = async () => {
    try {
      await googleSignIn();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <h1 className="text-center text-4xl font-bold py-8">LogIn</h1>
      <div className="flex justify-center max-w-7xl m-auto py-5">
        <button
          onClick={googleLogInHandler}
          className="flex justify-center bg-gray-200  border px-7 py-4 text-3xl rounded-lg hover:bg-slate-300 animate-bounce transition-all items-center"
        >
          <FcGoogle />
          &nbsp;GoogleButton
        </button>
      </div>
    </div>
  );
};

export default Login;
