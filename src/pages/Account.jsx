import React from "react";
import { UserAuth } from "../context/AuthContext";

const Account = () => {
  const { logout, user } = UserAuth();

  const signoutHandler = async () => {
    try {
      logout();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="w-96 m-auto">
      <h1 className="text-center text-4xl font-bold pt-12">Account</h1>
      <div className="flex justify-center flex-col my-28">
        <p>Welcome, {user?.displayName}</p>
        <p>your uid is: {user?.uid}</p>
      </div>
      <button onClick={signoutHandler} className="border py-2 px-5 mt-10">
        LogOut User
      </button>
    </div>
  );
};

export default Account;
