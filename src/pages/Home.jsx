import React from "react";

const Home = () => {
  return (
    <div className="text-center text-5xl font-bold py-16">
      The Home Page Google OAuth Using Firebase Authentication
    </div>
  );
};

export default Home;
