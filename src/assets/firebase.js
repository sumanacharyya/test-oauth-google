// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
import { getAuth } from "firebase/auth";

// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBqC2HTnHUZJPRAA85uE5k3R-4DjnZMJSs",
  authDomain: "test-oauth-9af92.firebaseapp.com",
  projectId: "test-oauth-9af92",
  storageBucket: "test-oauth-9af92.appspot.com",
  messagingSenderId: "26322263303",
  appId: "1:26322263303:web:8ad0576ed0ececb5ed39a2",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
