import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NavBar from "./components/NavBar";
import Login from "./pages/Login";
import Home from "./pages/Home";
import Account from "./pages/Account";
import { AuthContextProvider } from "./context/AuthContext";
import Protected from "./components/Protected";

function App() {
  return (
    <Router>
      <AuthContextProvider>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />

          <Route
            path="/account"
            element={
              <Protected>
                {" "}
                <Account />{" "}
              </Protected>
            }
          />

          <Route
            path="/*"
            element={
              <p className="text-9xl text-center"> 404, Page Not Found </p>
            }
          />
        </Routes>
      </AuthContextProvider>
    </Router>
  );
}

export default App;
